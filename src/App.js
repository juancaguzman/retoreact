import React from "react";
import "./App.css";

import { PageHeader, Row, Col, Avatar } from "antd";
import "antd/dist/antd.css";

import Body from "./componentes/Body";

function App() {
  return (
    <div className="App">
      <Row className="cabecera">
        <Col span={4}>
          <Avatar
            shape="square"
            src="https://site.krowdy.com/wp-content/uploads/2019/01/14153940/logoKrowdymenu.png"
            style={{
              color: "#f56a00",
              width: 90,
              height: 40
            }}
          />
        </Col>
        <Col span={4} offset={16}>
          <Col span={18}>
            <div
              style={{
                margin: 10
              }}
            >
              Walther Ayala Poma
            </div>
          </Col>
          <Col span={6}>
            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
          </Col>
        </Col>
      </Row>
      <Body />
    </div>
  );
}

export default App;

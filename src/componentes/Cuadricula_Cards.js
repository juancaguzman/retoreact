import React, { Component } from "react";
import img_not_found from "../notfound.jpg";
import "antd/dist/antd.css";
import "../Cuadricula_Cards.css";
import { Row, Col, Card, Icon } from "antd";
const { Meta } = Card;

export default class Ver_Preguntas extends Component {
  render() {
    const { Questions, changeSlider2 } = this.props;
    return (
      <div
        className="bloque_image"
        style={{
          padding: "30px"
        }}
      >
        <Row gutter={16}>
          {Questions.map((valor, i) => {
            return (
              <Col className="columna_matriz" span={8}>
                <Card
                  hoverable
                  style={{
                    width: 300
                  }}
                  cover={<img alt="example" src={img_not_found} />}
                >
                  <Meta title={valor.question} description={valor.number} />
                </Card>
                <div className="addVideo">
                  <a onClick={changeSlider2}>
                    <Icon
                      style={{
                        fontSize: "36px",
                        color: "#3498DB"
                      }}
                      type="plus-circle"
                      theme="filled"
                    />
                  </a>
                </div>
              </Col>
            );
          })}
        </Row>
      </div>
    );
  }
}

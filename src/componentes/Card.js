import React, { Component } from "react";
import Iconos_video from "./Iconos_video";

import "./Body.css";
import { Card } from "antd";
import "antd/dist/antd.css";
import { constants } from "os";

const myConstraints = {
  audio: false,
  video: {
    width: {
      min: 1280
    },
    height: {
      min: 720
    }
  }
};

export default class Body extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mediaStream: {},
      mediaRecorder: {},
      videos: [],
      chunks: [],
      playing: true,
      camara: "on",
      status: "play",
      show: true
    };

    this.fullBleedVideo = React.createRef();
    this.fullBleedVideo2 = React.createRef();

    // this.grabacion = this.grabacion.bind(this);
    // this.detenerGrabacion = this.detenerGrabacion.bind(this);
  }

  componentDidMount() {
    this.getMediaStram();
  }

  async getMediaStram() {
    var mediaStream = await navigator.mediaDevices.getUserMedia(myConstraints);
    if (mediaStream.active) {
      const mediaRecorder = new MediaRecorder(mediaStream);
      console.log("xd2", mediaRecorder);
      this.setState(
        {
          mediaStream: mediaStream,
          mediaRecorder: mediaRecorder
        },
        () => (this.fullBleedVideo.current.srcObject = mediaStream)
      );
    }
  }
  handleRecord = () => {
    const { status, mediaRecorder } = this.state;

    const video = document.getElementById("id_video2");
    if (status === "play") {
      this.fullBleedVideo.current.play();
      this.fullBleedVideo.current.style.display = "blue";
      mediaRecorder.start();

      console.log("video encendido");
      console.log("media", this.state.mediaRecorder);
      console.log(mediaRecorder.state);
      this.setState({
        status: "stop",
        show: false
      });
    } else if (status === "stop") {
      this.fullBleedVideo.current.pause();
      this.setState({
        status: "play"
      });
      mediaRecorder.stop();
      console.log(mediaRecorder.state);
      mediaRecorder.ondataavailable = async e => {
        await this.setState({
          chunks: e.data
        });
      };

      mediaRecorder.onstop = e => {
        const chunks = this.state.chunks;
        const blob = new Blob([chunks], { type: "vide/mp4;" });
        const videourl = window.URL.createObjectURL(blob);
        this.setState({
          videos: [...this.state.videos, videourl],
          chunks: [],
          status: "play",
          show: true
        });
        this.fullBleedVideo2.current.src = videourl;
        console.log("videos", this.state.videos);
      };
      this.state.show
        ? (this.fullBleedVideo.current.style.display = "block")
        : (this.fullBleedVideo.current.style.display = "none");
      this.state.show
        ? (this.fullBleedVideo2.current.style.display = "none")
        : (this.fullBleedVideo2.current.style.display = "block");
      this.state.show
        ? (this.fullBleedVideo2.current.controls = false)
        : (this.fullBleedVideo2.current.controls = true);
    }
  };

  render() {
    const { number, question } = this.props;
    const { camara } = this.state;
    return (
      <div>
        <Card
          className="Cards_carrousel"
          title={<div className="numeroPregunta"> {number} </div>}
          extra={<div className="pregunta"> {question} </div>}
        >
          {camara === "on" && (
            <div>
              <video
                className="elvideo"
                // autoPlay
                ref={this.fullBleedVideo}
                id="id_video"
              />
              <video
                className="elvideo2"
                // autoPlay
                ref={this.fullBleedVideo2}
                id="id_video2"
              />
              <div className="contenedor-icons" id="reproductor1">
                <Iconos_video type="pause" />
                {/* <Iconos_video type="reload" Click={this.grabacion(true)} /> */}
                <a onClick={this.handleRecord}>
                  <Iconos_video type="border" />
                </a>
                {/* <button onClick={this.handleRecord} class="btn">
                  start
                </button> */}
              </div>
            </div>
          )}

          {camara === "off" && (
            <div>
              <video className="elvideo" id="id_video2" />
              <div className="contenedor-icons" id="reproductor2">
                <Iconos_video type="pause" />
                <Iconos_video type="reload" />
                <Iconos_video type="border" />
              </div>
            </div>
          )}
        </Card>
      </div>
    );
  }

  // comenzarGrabacion = () => {
  //   console.log("Grabando...");
  //   const { mediaRecorder } = this.state;

  //   // const video2 = document.getElementById("reproductor2");

  //   mediaRecorder.start();
  //   // this.setState({
  //   //   status: "stop",
  //   //   show: false,
  //   //   msg: "STOP"
  //   // });
  // };

  // grabacion = band => {
  //   if (band) {
  //     console.log("Grabando...");
  //     const { mediaRecorder } = this.state;
  //     mediaRecorder.start();
  //   } else {
  //     console.log("deteniendo grabacion...");
  //     const { mediaRecorder } = this.state;

  //     const video2 = document.getElementById("reproductor2");

  //     mediaRecorder.stop();
  //     mediaRecorder.ondataavailable = async e => {
  //       await this.setState({
  //         chunks: e.data
  //       });
  //     };

  //     mediaRecorder.onstop = e => {
  //       const chunks = this.state.chunks;
  //       const blob = new Blob([chunks], { type: "vide/mp4;" });
  //       const videourl = window.URL.createObjectURL(blob);
  //       this.setState({
  //         videos: [...this.state.videos, videourl],
  //         chunks: [],
  //         camara: "off"
  //       });
  //       video2.src = videourl;
  //     };
  //   }
  // };
}

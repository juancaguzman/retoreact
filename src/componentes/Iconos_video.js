import React, { Component } from "react";

import "../Iconos_video.css";
import { Icon } from "antd";
import "antd/dist/antd.css";

export default class Iconos_video extends Component {
  render() {
    const { type, grabacion } = this.props;
    return (
      <div className="botones_video">
        <a onClick={grabacion}>
          <Icon
            style={{
              fontSize: "18px",
              color: "#fff"
            }}
            type={type}
            twoToneColor="#ffffff"
          />
        </a>{" "}
      </div>
    );
  }
}

import React, { Component } from "react";
import "antd/dist/antd.css";
import "../Video_Bienvenida.css";

export default class Ver_Preguntas extends Component {
  render() {
    const { changeSlider } = this.props;
    return (
      <div className="video_inicial">
        <iframe
          className="video-responsive"
          src="https://www.youtube.com/embed/z2QTxp7aOBU"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        />
        <div className="skipIntro">
          <a onClick={changeSlider}> Skip </a>
        </div>
      </div>
    );
  }
}

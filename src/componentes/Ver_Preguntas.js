import React, { Component } from "react";

import "../Ver_Preguntas.css";
import { Icon } from "antd";
import "antd/dist/antd.css";

export default class Ver_Preguntas extends Component {
  render() {
    const { changeMatriz } = this.props;
    return (
      <div className="showMatriz">
        <div>
          <div className="ver_preguntas"> Ver preguntas </div>
          <div className="ver_icon">
            <a onClick={changeMatriz}>
              <Icon
                style={{
                  fontSize: "26px",
                  color: "#fff"
                }}
                type="appstore"
                theme="filled"
              />
            </a>
          </div>
        </div>
      </div>
    );
  }
}

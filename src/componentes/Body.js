import React, { Component } from "react";
import { Questions } from "../Questions.json";

import "./Body.css";
import "../App.css";
// import img_not_found from "../notfound.jpg";

import Tarjeta from "./Card";
import Ver_Preguntas from "./Ver_Preguntas";
import Cuadricula_Cards from "./Cuadricula_Cards";
import Video_Bienvenida from "./Video_Bienvenida";

import { Layout, Menu, Icon, Col, Row, Progress, Carousel, Card } from "antd";

import "antd/dist/antd.css";

const { Header, Content, Footer, Sider } = Layout;
const { Meta } = Card;

export default class Body extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      Questions,
      nummero: 0,
      delante: 0,
      mediaStream: {},
      currentCard: -1
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.carousel = React.createRef();
    //this.fullBleedVideo = React.createRef();

    this.changeSlider = this.changeSlider.bind(this);
    this.onChange = this.onChange.bind(this);
    this.changeMatriz = this.changeMatriz.bind(this);
    this.changeSlider2 = this.changeSlider2.bind(this);
  }

  /*returnCards = () => {
    return this.state.Questions.map((question, i) => {
      return (
        <div>
          <Card
            className="Cards_carrousel"
            title={<p className="numeroPregunta"> {question.number} </p>}
            extra={<p className="pregunta"> {question.question} </p>}
          >
            <video className="elvideo" autoPlay ref={this.fullBleedVideo} />
          </Card>
        </div>
      );
    });
  };*/

  /*returnTagVideo = nroCard => {
    console.log("nroCard", nroCard);
    return (
      <video
        className="elvideo"
        autoPlay
        ref={this.fullBleedVideo}
        id="id_video"
      />
    );
  };*/

  onChange = a => {
    console.log(a);
    this.setState({
      currentCard: a
    });
  };

  next() {
    this.carousel.next();
  }
  previous() {
    this.carousel.prev();
  }

  render() {
    const props = {
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: false,
      speed: 2000,
      slidesToShow: 1,
      slidesToScroll: 1,
      accesibility: false,
      arrows: false
      // draggable:true
      // fade: true,
      // touchMove:false,className: "center",
      // centerMode: true,
      // centerPadding: "60px",
    };
    return (
      <Layout
        style={{
          minHeight: "100vh"
        }}
      >
        <Sider
          style={{
            background: "#FBFCFC"
          }}
          collapsible="collapsible"
          collapsed={!this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          <Menu
            style={{
              background: "#FBFCFC"
            }}
            defaultSelectedKeys={["1"]}
            mode="inline"
          >
            <Menu.Item key="1">
              <Icon type="menu-fold" />
              <span> Option 1 </span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="contacts" />
              <span> Option 2 </span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header
            style={{
              background: "#fff",
              padding: 0
            }}
          >
            <Col className="columna" span={2}>
              <div>
                <Icon type="arrow-left" />
              </div>
            </Col>
            <Col className="columna video_cuestionario" span={14}>
              <p className="titulo_ventana"> Video Cuestionario </p>
            </Col>
            <Col className="columna" span={8}>
              <Row>
                <Col span={8}> Preguntas resueltas </Col>
                <Col span={13}>
                  <Progress percent={50} showInfo={false} />
                </Col>
                <Col span={3}> 5 / {this.state.Questions.length} </Col>
              </Row>
            </Col>
          </Header>
          <Content>
            <div className="fondoUp" />

            {this.state.delante === 0 && (
              <Video_Bienvenida changeSlider={this.changeSlider} />
            )}

            {this.state.delante === 1 && (
              <>
                <Carousel
                  className="carrusel"
                  afterChange={this.onChange}
                  ref={node => (this.carousel = node)}
                  {...props}
                >
                  {this.state.Questions.map((question, i) => {
                    return <Tarjeta {...question} />;
                  })}
                </Carousel>
                <button onClick={this.previous}>prev</button>

                <button onClick={this.next}>next</button>
              </>
            )}

            {this.state.delante === 2 && (
              <Cuadricula_Cards
                Questions={this.state.Questions}
                changeSlider2={this.changeSlider2}
              />
            )}
            {this.state.delante === 1 && (
              <Ver_Preguntas changeMatriz={this.changeMatriz} />
            )}
          </Content>
        </Layout>
      </Layout>
    );
  }

  changeSlider() {
    this.setState({
      delante: this.state.delante + 1
    });
  }

  changeMatriz() {
    this.setState({
      delante: this.state.delante + 1
    });
  }

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({
      collapsed
    });
  };

  changeSlider2() {
    this.setState({
      delante: this.state.delante - 1
    });
  }

  onChange = a => {
    console.log(a);
    this.setState({
      currentCard: a
    });
  };
}
